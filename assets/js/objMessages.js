const objMessages = {
    "idOuName": {
        "erreurs": {
            "vide": "Message à afficher si l'élément de formulaire n'est pas complété",
            "motif": "Explication complète du motif attendu"
        }
    },

    "jeSuis": {
        "erreurs": {
            "vide": "Veuillez préciser votre identité de genre, s’il-vous-plaît."
        },
        "ok": "Parfait! passons à l'étape suivante..."
    },

    "jeCherche": {
        "erreurs": {
            "vide": "Veuillez préciser votre recherche, s’il-vous-plaît."
        },
        "ok": "Parfait! passons à l'étape suivante..."
    },

    "dateNaissance": {
        "erreurs": {
            "vide": "Veuillez entrer votre date de naissance, s’il-vous-plaît. ",
            "motif": "Vous êtes trop jeune pour vous inscrire ici!",
            "type": {
                "jour": "Entrez votre jour de naissance",
                "mois": "Sélectionnez votre mois de naissance",
                "annee": "Entrez les 4 chiffres de votre année de naissance"
            }
        }
    },

    "codePostal": {
        "erreurs": {
            "vide": "Veuillez entrer votre code postal pour nous permettre d'identifier votre lieu de résidence.",

        }
    },

    "pseudo": {
        "erreurs": {
            "vide": "Veuiller compléter le champ pseudo, s’il-vous-plaît.",
            "dejaPris": "Ce pseudo existe déjà... Essayez autre chose...",
            "motif": "Attention, votre nom d'usager doit comporter un minimum de 2 lettres. Trait d'union ou espace sont permis pour séparer deux mots."
        }
    },

    "courriel": {
        "erreurs": {
            "vide": "Veuillez entrer votre adresse courriel, s’il-vous-plaît.",
            "motif": "Vérifiez votre adresse courriel, il semble y avoir une erreur."
        }
    },

    "motDePasse": {
        "erreurs": {
            "vide": "Veuillez définir votre mot de passe.",
            "motif": "Votre mot de passe ne respecte pas encore tous les critères... Renforcez-le!",
            "base": "Pour renforcer la sécurité de votre mot de passe: \n",
            "type": {
                "size": "\t  -  saisissez entre 6 et 10 caractères\n",
                "minus": "\t  -  ajoutez au moins une minuscule\n",
                "majus": "\t  -  ajoutez au moins une majuscule\n",
                "num": "\t  -  ajoutez au moins un chiffre\n"
            }
        }
    },

    "consentement": {
        "erreurs": {
            "vide": "SVP, veuillez prendre connaissance et approuvez les termes de l'entente."
        }
    }
}