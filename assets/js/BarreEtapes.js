/**
 * Structure HTML du Formulaire Multi-Étapes
 *
 * form.formMultiStep
 *      div ou nav (si navigable) > ol.stepsBar > li.stepsBar__title#step1title
 *      // une section pour chaque étape de formulaire
 *      section[aria-labelledby ='step1title']
 */
define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var BarreEtapes = /** @class */ (function () {
        function BarreEtapes() {
            this.formulaire = document.querySelector('.formMultiSteps');
            // Liste des conteneurs d'étapes du formulaire <section>
            this.arrSteps = Array.apply(null, document.querySelectorAll('.formMultiSteps__step'));
            this.etapeEnCours = 0; // indice 0 = Étape 1
            this.arrEtapeFini = new Array(false, false, false);
            // private boutonPrecedent:HTMLButtonElement;
            this.etapePrecedente = 0;
            this.sensChangement = 0;
            // ajouter le HTML de la Barre de progression d'étapes
            this.stepsBar = document.createElement('div');
            this.stepsBar.setAttribute('aria-label', 'Formulaire en plusieurs étapes');
            this.stepsBar.innerHTML = "<ol class=\"stepsBar\">\n            <li class=\"stepsBar__title\"  id=\"step1title\"> <span>1.</span> Qui?        </li>\n            <li class=\"stepsBar__title\"  id=\"step2title\"> <span>2.</span> Mon profil  </li>\n            <li class=\"stepsBar__title\"  id=\"step3title\"> <span>3.</span> Mon compte   </li>\n            </ol>";
            this.formulaire.insertBefore(this.stepsBar, this.arrSteps[0]);
            this.arrStepsBarTitle = Array.apply(null, document.querySelectorAll('.stepsBar__title'));
            // initialiser la progression en étapes appeler une méthode afficherEtapeCourante(noEtape)
            this.afficherEtapeCourante(this.etapeEnCours);
        }
        BarreEtapes.prototype.afficherEtapeCourante = function (numero) {
            // ...
            this.arrSteps.forEach(function (etape) { return etape.className = "formMultiSteps__step formMultiSteps__step--is-invisible"; });
            this.arrSteps[numero].className = "formMultiSteps__step formMultiSteps__step--is-visible";
            this.arrStepsBarTitle[numero].setAttribute('aria-current', 'step');
            this.afficherBoutonSuivant();
            // if(numero>0)
            // {
            //     this.afficherBoutonPrecedent();
            // }
            // ...
        };
        BarreEtapes.prototype.afficherBoutonSuivant = function () {
            if (this.boutonSuivant) {
                this.boutonSuivant.removeEventListener("click", this.changerEtape.bind(this));
                this.arrSteps[this.etapePrecedente].removeChild(this.boutonSuivant);
            }
            if (this.etapeEnCours < 2) {
                this.sensChangement = 1;
                this.boutonSuivant = document.querySelector('.suivant .formMultiSteps__btn[type=button]');
                this.desactiverBoutonSuivant(this.etapeEnCours);
                this.arrSteps[this.etapeEnCours].appendChild(this.boutonSuivant);
                this.boutonSuivant.addEventListener("click", this.changerEtape.bind(this));
            }
            else {
                console.log("dernier etape");
                this.boutonSubmit = document.querySelector('.formMultiSteps__btn[type=submit]');
                console.log(this.boutonSubmit);
                this.desactiverBoutonSuivant(this.etapeEnCours);
                this.arrSteps[this.etapeEnCours].appendChild(this.boutonSubmit);
                this.boutonSubmit.addEventListener("click", this.changerEtape.bind(this));
            }
        };
        BarreEtapes.prototype.changerEtape = function (evenement) {
            console.log("allo");
            console.log(this.sensChangement);
            this.arrStepsBarTitle[this.etapeEnCours].removeAttribute('aria-current');
            this.arrStepsBarTitle[this.etapeEnCours].className = "stepsBar__title is-completed";
            this.etapePrecedente = this.etapeEnCours;
            this.etapeEnCours = this.etapeEnCours + this.sensChangement;
            console.log(this.etapeEnCours);
            this.afficherEtapeCourante(this.etapeEnCours);
        };
        BarreEtapes.prototype.activerBoutonSuivant = function (numeroEtapeFinie) {
            console.log(this.etapeEnCours);
            if (this.etapeEnCours < 2) {
                this.boutonSuivant.removeAttribute('disabled');
                this.arrEtapeFini[numeroEtapeFinie] = true;
            }
            else {
                this.boutonSubmit.removeAttribute('disabled');
                this.arrEtapeFini[this.etapeEnCours] = true;
            }
        };
        BarreEtapes.prototype.desactiverBoutonSuivant = function (numEtape) {
            console.log("dedans");
            if (numEtape < 2) {
                this.boutonSuivant.setAttribute('disabled', 'true');
            }
            else {
                this.boutonSubmit.setAttribute('disabled', 'true');
            }
            // bouton.setAttribute('disabled', 'true');
        };
        return BarreEtapes;
    }());
    exports.BarreEtapes = BarreEtapes;
});
//# sourceMappingURL=BarreEtapes.js.map