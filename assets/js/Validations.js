define(["require", "exports", "./BarreEtapes"], function (require, exports, BarreEtapes_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Validations = /** @class */ (function () {
        // Constructeur
        function Validations(objetJSON) {
            this.blnJourCorrect = true;
            this.blnMoisCorrect = true;
            this.blnAnneeCorrect = true;
            this.intEtapeAPasser = 1;
            this.barreEtape = null;
            // -- Éléments de formulaire à valider
            // Étape 1
            this.refarrJeSuis = Array.apply(null, document.querySelectorAll('[name=jeSuis]'));
            this.refarrJeCherche = Array.apply(null, document.querySelectorAll('.jeCherche'));
            this.refJourNaissance = document.querySelector('#jourNaissance');
            this.refMoisNaissance = document.querySelector('#moisNaissance');
            this.refAnneeNaissance = document.querySelector('#anneeNaissance');
            this.refCodePostal = document.querySelector('#codePostal');
            this.refPseudo = document.querySelector('#pseudo');
            this.refCourriel = document.querySelector('#courriel');
            this.refMotPasse = document.querySelector('#motPasse');
            this.refConsentement = document.querySelector('#consentement');
            this.refAfficherMotPasse = document.querySelector('#afficherMotPasse');
            this.arrEtapes = {
                etape1: [false, false],
                etape2: [false, false],
                etape3: [false, false, false, false],
            };
            document.querySelector('form').noValidate = true;
            this.objMessages = objetJSON;
            console.log(this.objMessages);
            this.initialiser();
        }
        Validations.prototype.initialiser = function () {
            var _this = this;
            this.barreEtape = new BarreEtapes_1.BarreEtapes();
            this.refarrJeSuis.forEach(function (btnRadio) { return btnRadio.addEventListener("blur", _this.validerJeSuis.bind(_this)); });
            this.refarrJeCherche.forEach(function (bCocher) { return bCocher.addEventListener("blur", _this.validerJeCherche.bind(_this)); });
            this.refarrJeCherche.forEach(function (bCocher) { return bCocher.addEventListener("click", _this.validerJeCherche.bind(_this)); });
            this.refJourNaissance.addEventListener("blur", this.validerJourNaissance.bind(this));
            this.refMoisNaissance.addEventListener("blur", this.validerMoisNaissance.bind(this));
            this.refAnneeNaissance.addEventListener("blur", this.validerAnneeNaissance.bind(this));
            this.refCodePostal.addEventListener("blur", this.validerCodePostal.bind(this));
            this.refPseudo.addEventListener("blur", this.validerPseudo.bind(this));
            this.refCourriel.addEventListener("blur", this.validerCourriel.bind(this));
            this.refMotPasse.addEventListener("blur", this.validerMotDePasse.bind(this));
            this.refConsentement.addEventListener("blur", this.validerConsentement.bind(this));
            this.refAfficherMotPasse.addEventListener("click", this.basculerMotPasse.bind(this));
        };
        // Méthodes de validation
        Validations.prototype.validerJeSuis = function (evenement) {
            var element = evenement.currentTarget;
            var intSection = 0;
            var strEtape = "etape1";
            var strMessage = "";
            if (element.checked) {
                strMessage = this.objMessages.jeSuis.ok;
                this.arrEtapes.etape1[0] = true;
            }
            else {
                this.intEtapeAPasser = 1;
                this.arrEtapes.etape1[0] = false;
                strMessage = this.objMessages.jeSuis.erreurs.vide;
            }
            this.validerSection(strEtape, intSection);
            this.afficherErreur(element, strMessage);
        };
        Validations.prototype.validerJeCherche = function (evenement) {
            var element = evenement.currentTarget;
            var intSection = 0;
            var strEtape = "etape1";
            var strMessage = "";
            var blnCocher = this.verifierCheckbox(element);
            if (blnCocher == true) {
                this.arrEtapes.etape1[1] = true;
                strMessage = this.objMessages.jeCherche.ok;
            }
            else {
                this.intEtapeAPasser = 1;
                this.arrEtapes.etape1[1] = false;
                strMessage = this.objMessages.jeCherche.erreurs.vide;
            }
            this.validerSection(strEtape, intSection);
            this.afficherErreur(element, strMessage);
        };
        Validations.prototype.validerJourNaissance = function (evenement) {
            var element = evenement.currentTarget;
            this.blnJourCorrect = this.verifierSiVide(element);
            if (this.blnJourCorrect == true) {
                this.intJourNaissance = parseInt(element.value);
            }
            this.validerDate(element);
        };
        Validations.prototype.validerMoisNaissance = function (evenement) {
            var element = evenement.currentTarget;
            if (element.value == "00") {
                this.blnMoisCorrect = false;
            }
            else {
                this.intMoisNaissance = parseInt(element.value);
            }
            this.validerDate(element);
        };
        Validations.prototype.validerAnneeNaissance = function (evenement) {
            var element = evenement.currentTarget;
            var strMessage = "";
            var resultatVide = true;
            var resultatPattern = true;
            resultatVide = this.verifierSiVide(element);
            if (resultatVide == false) {
                this.blnAnneeCorrect = false;
            }
            else {
                resultatPattern = this.verifierPattern(element, element.pattern);
                if (resultatPattern == false) {
                    this.blnAnneeCorrect = false;
                }
                else {
                    this.intAnneeNaissance = parseInt(element.value);
                }
            }
            this.validerDate(element);
        };
        Validations.prototype.validerDate = function (element) {
            var strMessage = "";
            var intSection = 1;
            var strEtape = "etape2";
            var jour = this.intJourNaissance;
            var mois = this.intMoisNaissance;
            var annee = this.intAnneeNaissance;
            if (this.blnJourCorrect == false || this.blnMoisCorrect == false || this.blnAnneeCorrect == false) {
                strMessage = this.objMessages.dateNaissance.erreurs.vide;
                if (this.blnJourCorrect == false) {
                    strMessage = strMessage + "<br/>" + this.objMessages.dateNaissance.erreurs.type.jour;
                }
                if (this.blnMoisCorrect == false) {
                    strMessage = strMessage + "<br/>" + this.objMessages.dateNaissance.erreurs.type.mois;
                }
                if (this.blnAnneeCorrect == false) {
                    strMessage = strMessage + "<br/>" + this.objMessages.dateNaissance.erreurs.type.annee;
                }
            }
            else {
                var dateNaissance = annee + "-" + mois + "-" + jour;
                var resultatAge = this.verifierAge(dateNaissance);
                if (resultatAge == false) {
                    strMessage = this.objMessages.dateNaissance.erreurs.motif;
                }
            }
            if (strMessage != "") {
                this.arrEtapes.etape2[0] = false;
                this.validerSection(strEtape, intSection);
                this.afficherErreur(element, strMessage);
            }
            else {
                this.arrEtapes.etape2[0] = true;
                this.validerSection(strEtape, intSection);
                this.effacerErreur(element);
            }
        };
        Validations.prototype.validerCodePostal = function (evenement) {
            var element = evenement.currentTarget;
            var intSection = 1;
            var strEtape = "etape2";
            var strMessage = "";
            var resultatVide = true;
            var resultatPattern = true;
            resultatVide = this.verifierSiVide(element);
            if (resultatVide == false) {
                strMessage = this.objMessages.codePostal.erreurs.vide;
            }
            else {
                resultatPattern = this.verifierPattern(element, element.pattern);
                if (resultatPattern == false) {
                    strMessage = this.objMessages.codePostal.erreurs.motif;
                }
            }
            if (strMessage != "") {
                this.arrEtapes.etape2[1] = false;
                this.validerSection(strEtape, intSection);
                this.afficherErreur(element, strMessage);
            }
            else {
                this.arrEtapes.etape2[1] = true;
                this.validerSection(strEtape, intSection);
                this.effacerErreur(element);
            }
        };
        Validations.prototype.validerPseudo = function (evenement) {
            var element = evenement.currentTarget;
            var intSection = 2;
            var strEtape = "etape3";
            var strMessage = "";
            var resultatVide = true;
            var resultatPattern = true;
            resultatVide = this.verifierSiVide(element);
            if (resultatVide == false) {
                strMessage = this.objMessages.pseudo.erreurs.vide;
            }
            else {
                resultatPattern = this.verifierPattern(element, element.pattern);
                if (resultatPattern == false) {
                    strMessage = this.objMessages.pseudo.erreurs.motif;
                }
            }
            if (strMessage != "") {
                this.arrEtapes.etape3[0] = false;
                this.validerSection(strEtape, intSection);
                this.afficherErreur(element, strMessage);
            }
            else {
                this.arrEtapes.etape3[0] = true;
                this.validerSection(strEtape, intSection);
                this.effacerErreur(element);
            }
        };
        Validations.prototype.validerCourriel = function (evenement) {
            var element = evenement.currentTarget;
            var intSection = 2;
            var strEtape = "etape3";
            var strMessage = "";
            var resultatVide = true;
            var resultatPattern = true;
            resultatVide = this.verifierSiVide(element);
            if (resultatVide == false) {
                strMessage = this.objMessages.courriel.erreurs.vide;
            }
            else {
                resultatPattern = this.verifierPattern(element, element.pattern);
                if (resultatPattern == false) {
                    strMessage = this.objMessages.courriel.erreurs.motif;
                }
            }
            if (strMessage != "") {
                this.arrEtapes.etape3[1] = false;
                this.validerSection(strEtape, intSection);
                this.afficherErreur(element, strMessage);
            }
            else {
                this.arrEtapes.etape3[1] = true;
                this.validerSection(strEtape, intSection);
                this.effacerErreur(element);
            }
        };
        Validations.prototype.validerMotDePasse = function (evenement) {
            var element = evenement.currentTarget;
            var intSection = 2;
            var strEtape = "etape3";
            var strValeur = element.value;
            var strMessage = "";
            var strMessageErreur = "";
            var resultatVide = true;
            var resultatPattern = true;
            var strPattern = "";
            resultatVide = this.verifierSiVide(element);
            if (resultatVide == false) {
                strMessage = this.objMessages.motDePasse.erreurs.vide;
            }
            else {
                if (strValeur < 6 || strValeur > 10) {
                    strMessage = strMessage + '<br/>' + this.objMessages.motDePasse.erreurs.type.size;
                }
                strPattern = "[a-z]";
                resultatPattern = this.verifierPattern(element, strPattern);
                if (resultatPattern == false) {
                    strMessage = strMessage + '<br/>' + this.objMessages.motDePasse.erreurs.type.minus;
                }
                strPattern = "[A-Z]";
                resultatPattern = this.verifierPattern(element, strPattern);
                if (resultatPattern == false) {
                    strMessage = strMessage + '<br/>' + this.objMessages.motDePasse.erreurs.type.majus;
                }
                strPattern = "[0-9]";
                resultatPattern = this.verifierPattern(element, strPattern);
                if (resultatPattern == false) {
                    strMessage = strMessage + '<br/>' + this.objMessages.motDePasse.erreurs.type.num;
                }
            }
            if (strMessage != "") {
                this.arrEtapes.etape3[2] = false;
                this.validerSection(strEtape, intSection);
                strMessageErreur = this.objMessages.motDePasse.erreurs.motif + '<br/>' +
                    this.objMessages.motDePasse.erreurs.base + strMessage;
                this.afficherErreur(element, strMessageErreur);
            }
            else {
                this.arrEtapes.etape3[2] = true;
                this.validerSection(strEtape, intSection);
                this.effacerErreur(element);
            }
        };
        Validations.prototype.validerConsentement = function (evenement) {
            var strMessage = "";
            var element = evenement.currentTarget;
            var intSection = 2;
            var strEtape = "etape3";
            if (element.checked == false) {
                strMessage = this.objMessages.consentement.erreurs.vide;
            }
            if (strMessage != "") {
                this.arrEtapes.etape3[3] = false;
                this.validerSection(strEtape, intSection);
                this.afficherErreur(element, strMessage);
            }
            else {
                this.arrEtapes.etape3[3] = true;
                this.validerSection(strEtape, intSection);
                this.effacerErreur(element);
            }
        };
        // Méthodes utilitaires
        Validations.prototype.verifierSiVide = function (element) {
            if (element.value == "") {
                return false;
            }
            else {
                return true;
            }
        };
        Validations.prototype.verifierPattern = function (element, strPattern) {
            var regex = new RegExp(strPattern);
            return regex.test(element.value);
        };
        Validations.prototype.afficherErreur = function (element, strMessage) {
            var conteneurFormulaire = element.closest('.ctnForm');
            var conteneurErreur = conteneurFormulaire.querySelector('.erreur');
            conteneurErreur.innerHTML = strMessage;
        };
        Validations.prototype.effacerErreur = function (element) {
            var conteneurFormulaire = element.closest('.ctnForm');
            var conteneurErreur = conteneurFormulaire.querySelector('.erreur');
            conteneurErreur.innerHTML = "";
        };
        Validations.prototype.verifierCheckbox = function (element) {
            var blnCocher = false;
            var conteneurFormulaire = element.closest('.ctnForm');
            var conteneurCheckbox = conteneurFormulaire.querySelectorAll('.jeCherche');
            for (var intCpt = 0; intCpt < conteneurCheckbox.length; intCpt++) {
                if (conteneurCheckbox[intCpt].checked) {
                    blnCocher = true;
                }
            }
            return blnCocher;
        };
        Validations.prototype.basculerMotPasse = function (evenement) {
            var cible = document.getElementById('motPasse');
            if (cible.type == 'password') {
                cible.type = 'text';
            }
            else {
                cible.type = 'password';
            }
        };
        Validations.prototype.verifierAge = function (dateNaissance) {
            var dateAnniversaire = new Date(dateNaissance);
            var dateAnniversairePourAgeMinimum = this.formaterDateMax(new Date());
            return dateAnniversairePourAgeMinimum.getTime() >= dateAnniversaire.getTime();
        };
        Validations.prototype.formaterDateMax = function (date) {
            var dateButoir = new Date(date);
            console.log("Nous sommes le : " + dateButoir);
            dateButoir.setFullYear(dateButoir.getFullYear() - 18);
            console.log("L'ann\u00E9e de naissance maximum est: " + dateButoir.getFullYear());
            var mois = '' + (dateButoir.getMonth() + 1), jour = '' + dateButoir.getDate(), annee = dateButoir.getFullYear();
            if (mois.length < 2)
                mois = '0' + mois;
            if (jour.length < 2)
                jour = '0' + jour;
            console.log("La date compl\u00E8te maximum est: " + [annee, mois, jour].join('-'));
            return new Date([annee, mois, jour].join('-'));
        };
        Validations.prototype.validerSection = function (strEtape, intSection) {
            if (this.arrEtapes[strEtape].indexOf(false) == -1) {
                this.barreEtape.activerBoutonSuivant(intSection);
            }
            else {
                this.barreEtape.desactiverBoutonSuivant(intSection);
            }
        };
        return Validations;
    }());
    exports.Validations = Validations;
});
//# sourceMappingURL=Validations.js.map