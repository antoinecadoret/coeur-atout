/**
 *
 */
import {BarreEtapes} from "./BarreEtapes";

export class Validations {

    // ATTRIBUTS
    private objMessages: JSON;
    private blnJourCorrect: boolean = true;
    private blnMoisCorrect: boolean = true;
    private blnAnneeCorrect:boolean = true;
    private intJourNaissance;
    private intMoisNaissance;
    private intAnneeNaissance;
    private intEtapeAPasser=1;
    private barreEtape:BarreEtapes = null;
    // -- Éléments de formulaire à valider
    // Étape 1
    private refarrJeSuis: Array<HTMLElement> = Array.apply(null, document.querySelectorAll('[name=jeSuis]'));
    private refarrJeCherche: Array<HTMLElement> = Array.apply(null, document.querySelectorAll('.jeCherche'));
    private refJourNaissance= document.querySelector('#jourNaissance');
    private refMoisNaissance = document.querySelector('#moisNaissance');
    private refAnneeNaissance = document.querySelector('#anneeNaissance');
    private refCodePostal = document.querySelector('#codePostal');
    private refPseudo = document.querySelector('#pseudo');
    private refCourriel = document.querySelector('#courriel');
    private refMotPasse = document.querySelector('#motPasse');
    private refConsentement = document.querySelector('#consentement');
    private refAfficherMotPasse = document.querySelector('#afficherMotPasse');
    private arrEtapes: any = {
        etape1: [false, false],
        etape2: [false, false], // seulement une booléenne pour la date anniversaire
        etape3: [false, false, false, false],
    };

    // Constructeur
    constructor(objetJSON: JSON){

        document.querySelector('form').noValidate = true;
        this.objMessages = objetJSON;
        console.log(this.objMessages);
        this.initialiser();

    }
    public initialiser(){
        this.barreEtape= new BarreEtapes();
        this.refarrJeSuis.forEach(btnRadio=>btnRadio.addEventListener("blur",this.validerJeSuis.bind(this)));
        this.refarrJeCherche.forEach(bCocher=>bCocher.addEventListener("blur",this.validerJeCherche.bind(this)));
        this.refarrJeCherche.forEach(bCocher=>bCocher.addEventListener("click",this.validerJeCherche.bind(this)));
        this.refJourNaissance.addEventListener("blur",this.validerJourNaissance.bind(this));
        this.refMoisNaissance.addEventListener("blur",this.validerMoisNaissance.bind(this));
        this.refAnneeNaissance.addEventListener("blur",this.validerAnneeNaissance.bind(this));
        this.refCodePostal.addEventListener("blur",this.validerCodePostal.bind(this));
        this.refPseudo.addEventListener("blur",this.validerPseudo.bind(this));
        this.refCourriel.addEventListener("blur",this.validerCourriel.bind(this));
        this.refMotPasse.addEventListener("blur",this.validerMotDePasse.bind(this));
        this.refConsentement.addEventListener("blur",this.validerConsentement.bind(this));
        this.refAfficherMotPasse.addEventListener("click",this.basculerMotPasse.bind(this));
    }

    // Méthodes de validation
    private validerJeSuis (evenement):void{
      const element =evenement.currentTarget;
      const intSection = 0;
      const strEtape ="etape1";
      const strMessage = "";
      if(element.checked)
      {
          strMessage = this.objMessages.jeSuis.ok;
          this.arrEtapes.etape1[0]=true;
      }
      else
      {
          this.intEtapeAPasser=1;
          this.arrEtapes.etape1[0]=false;

          strMessage = this.objMessages.jeSuis.erreurs.vide;
      }
      this.validerSection(strEtape, intSection);
      this.afficherErreur(element,strMessage);
    }
    private validerJeCherche (evenement):void{
        const element =evenement.currentTarget;
        const intSection = 0;
        const strEtape ="etape1";
        const strMessage = "";
        const blnCocher = this.verifierCheckbox(element);
        if(blnCocher==true){
                this.arrEtapes.etape1[1]=true;
            strMessage = this.objMessages.jeCherche.ok;
        }
        else
        {
            this.intEtapeAPasser=1;
            this.arrEtapes.etape1[1]=false;

            strMessage = this.objMessages.jeCherche.erreurs.vide;
        }
        this.validerSection(strEtape, intSection);
        this.afficherErreur(element,strMessage);

    }
    private validerJourNaissance (evenement):void{
        const element = evenement.currentTarget;
        this.blnJourCorrect = this.verifierSiVide(element);
        if(this.blnJourCorrect==true)
        {
            this.intJourNaissance = parseInt(element.value);
        }
        this.validerDate(element);
    }
    private validerMoisNaissance (evenement):void{
        const element=evenement.currentTarget;
        if(element.value == "00")
        {
            this.blnMoisCorrect = false
        }
        else
        {
            this.intMoisNaissance = parseInt(element.value);
        }
        this.validerDate(element);
    }
    private validerAnneeNaissance (evenement):void{
        const element=evenement.currentTarget;
        var strMessage:string ="";
        var resultatVide:boolean = true;
        var resultatPattern:boolean = true;
        resultatVide = this.verifierSiVide(element);
        if(resultatVide==false){
           this.blnAnneeCorrect = false;
        }
        else
        {
            resultatPattern = this.verifierPattern(element,element.pattern);
            if(resultatPattern == false)
            {
               this.blnAnneeCorrect = false;
            }
            else
            {
                this.intAnneeNaissance= parseInt(element.value);
            }
        }
        this.validerDate(element);

    }
    private validerDate(element){
        var strMessage:string = "";
        const intSection = 1;
        const strEtape= "etape2";
        const jour = this.intJourNaissance;
        const mois = this.intMoisNaissance;
        const annee = this.intAnneeNaissance;
        if(this.blnJourCorrect==false|| this.blnMoisCorrect==false || this.blnAnneeCorrect==false)
        {
            strMessage = this.objMessages.dateNaissance.erreurs.vide;
            if(this.blnJourCorrect==false)
            {
                strMessage = strMessage + "<br/>" + this.objMessages.dateNaissance.erreurs.type.jour;
            }
            if(this.blnMoisCorrect==false)
            {
                strMessage = strMessage + "<br/>" + this.objMessages.dateNaissance.erreurs.type.mois;
            }
            if(this.blnAnneeCorrect==false)
            {
                strMessage = strMessage + "<br/>" + this.objMessages.dateNaissance.erreurs.type.annee;
            }
        }
        else
        {
            const dateNaissance = `${annee}-${mois}-${jour}`;
            const resultatAge = this.verifierAge(dateNaissance);
            if(resultatAge==false)
            {
                strMessage = this.objMessages.dateNaissance.erreurs.motif;
            }

        }
        if(strMessage != ""){
            this.arrEtapes.etape2[0]=false;
            this.validerSection(strEtape, intSection);
            this.afficherErreur(element,strMessage);
        }
        else
        {
            this.arrEtapes.etape2[0]=true;
            this.validerSection(strEtape, intSection);
            this.effacerErreur(element);
        }
    }
    private validerCodePostal (evenement):void{
        const element = evenement.currentTarget;
        const intSection = 1;
        const strEtape = "etape2";
        var strMessage:string ="";
        var resultatVide:boolean = true;
        var resultatPattern:boolean = true;
        resultatVide = this.verifierSiVide(element);
        if(resultatVide==false){
            strMessage = this.objMessages.codePostal.erreurs.vide;
        }
        else
        {
            resultatPattern = this.verifierPattern(element,element.pattern);
            if(resultatPattern == false){
                strMessage = this.objMessages.codePostal.erreurs.motif;
            }
        }
        if(strMessage != "")
        {
            this.arrEtapes.etape2[1]=false;
            this.validerSection(strEtape, intSection);
            this.afficherErreur(element, strMessage);
        }
        else
        {
            this.arrEtapes.etape2[1]=true;
            this.validerSection(strEtape, intSection);
            this.effacerErreur(element);
        }
    }
    private validerPseudo (evenement):void{
        const element = evenement.currentTarget;
        const intSection = 2;
        const strEtape = "etape3";
        var strMessage:string ="";
        var resultatVide:boolean = true;
        var resultatPattern:boolean = true;
        resultatVide = this.verifierSiVide(element);
        if(resultatVide==false){
            strMessage = this.objMessages.pseudo.erreurs.vide;
        }
        else
        {
            resultatPattern = this.verifierPattern(element,element.pattern);
            if(resultatPattern == false){
                strMessage = this.objMessages.pseudo.erreurs.motif;
            }
        }
        if(strMessage != "")
        {
            this.arrEtapes.etape3[0]=false;
            this.validerSection(strEtape, intSection);
            this.afficherErreur(element, strMessage);
        }
        else
        {
            this.arrEtapes.etape3[0]=true;
            this.validerSection(strEtape, intSection);
            this.effacerErreur(element);
        }

    }
    private validerCourriel (evenement):void{
        const element = evenement.currentTarget;
        const intSection = 2;
        const strEtape = "etape3";
        var strMessage:string ="";
        var resultatVide:boolean = true;
        var resultatPattern:boolean = true;
        resultatVide = this.verifierSiVide(element);
        if(resultatVide==false){
            strMessage = this.objMessages.courriel.erreurs.vide;
        }
        else
        {
            resultatPattern = this.verifierPattern(element,element.pattern);
            if(resultatPattern == false){
                strMessage = this.objMessages.courriel.erreurs.motif;
            }
        }
        if(strMessage != "")
        {
            this.arrEtapes.etape3[1]=false;
            this.validerSection(strEtape, intSection);
            this.afficherErreur(element, strMessage);
        }
        else
        {
            this.arrEtapes.etape3[1]=true;
            this.validerSection(strEtape, intSection);
            this.effacerErreur(element);
        }
    }
    private validerMotDePasse (evenement):void{
        const element = evenement.currentTarget;
        const intSection = 2;
        const strEtape = "etape3";
        const strValeur = element.value;
        var strMessage:string ="";
        var strMessageErreur:string = "";
        var resultatVide:boolean = true;
        var resultatPattern:boolean = true;
        var strPattern="";
        resultatVide = this.verifierSiVide(element);
        if(resultatVide==false){
            strMessage = this.objMessages.motDePasse.erreurs.vide;
        }
        else
        {
            if(strValeur<6 || strValeur>10){
                strMessage = strMessage + '<br/>' + this.objMessages.motDePasse.erreurs.type.size;
            }
            strPattern="[a-z]";
            resultatPattern = this.verifierPattern(element,strPattern);
            if(resultatPattern == false){
                strMessage = strMessage + '<br/>' + this.objMessages.motDePasse.erreurs.type.minus;
            }
            strPattern="[A-Z]";
            resultatPattern = this.verifierPattern(element,strPattern);
            if(resultatPattern == false){
                strMessage = strMessage + '<br/>' + this.objMessages.motDePasse.erreurs.type.majus;
            }
            strPattern="[0-9]";
            resultatPattern = this.verifierPattern(element,strPattern);
            if(resultatPattern == false){
                strMessage = strMessage + '<br/>' + this.objMessages.motDePasse.erreurs.type.num;
            }
        }
        if(strMessage != "")
        {
            this.arrEtapes.etape3[2]=false;
            this.validerSection(strEtape, intSection);
            strMessageErreur = this.objMessages.motDePasse.erreurs.motif + '<br/>' +
                              this.objMessages.motDePasse.erreurs.base + strMessage;
            this.afficherErreur(element, strMessageErreur);
        }
        else
        {
            this.arrEtapes.etape3[2]=true;
            this.validerSection(strEtape, intSection);
            this.effacerErreur(element);
        }
    }
    private validerConsentement (evenement):void{
        var strMessage =  "";
        const element = evenement.currentTarget;
        const intSection = 2;
        const strEtape = "etape3";
        if(element.checked == false){
            strMessage = this.objMessages.consentement.erreurs.vide;
        }
        if( strMessage != "")
        {
            this.arrEtapes.etape3[3]=false;
            this.validerSection(strEtape, intSection);
            this.afficherErreur(element, strMessage);
        }
        else
        {
            this.arrEtapes.etape3[3]=true;
            this.validerSection(strEtape, intSection);
            this.effacerErreur(element);
        }
    }
    // Méthodes utilitaires
    private  verifierSiVide(element){
        if(element.value=="")
        {
            return false;
        }
        else
        {
            return true;
        }

    }
    private verifierPattern(element,strPattern):boolean{
        var regex = new RegExp(strPattern);
        return regex.test(element.value);
    }
    private afficherErreur(element, strMessage){
        const conteneurFormulaire = element.closest('.ctnForm');
        const conteneurErreur = conteneurFormulaire.querySelector('.erreur');
            conteneurErreur.innerHTML= strMessage;

    }
    private effacerErreur(element){
        const conteneurFormulaire = element.closest('.ctnForm');
        const conteneurErreur = conteneurFormulaire.querySelector('.erreur');
        conteneurErreur.innerHTML= "";
    }
    private verifierCheckbox(element):boolean{
        var blnCocher=false;
        const conteneurFormulaire = element.closest('.ctnForm');
        const conteneurCheckbox = conteneurFormulaire.querySelectorAll('.jeCherche');
        for(var intCpt = 0; intCpt<conteneurCheckbox.length; intCpt ++)
        {
            if(conteneurCheckbox[intCpt].checked)
            {
                blnCocher=true;
            }
        }
        return blnCocher;
    }
    private basculerMotPasse(evenement):void{
        let cible = <HTMLInputElement>document.getElementById('motPasse');
        if(cible.type == 'password')
        {
            cible.type='text';
        }
        else
        {
            cible.type='password';
        }
    }
    private verifierAge(dateNaissance):boolean{
            const dateAnniversaire = new Date(dateNaissance);
            const dateAnniversairePourAgeMinimum = this.formaterDateMax(new Date());
            return dateAnniversairePourAgeMinimum.getTime() >= dateAnniversaire.getTime();

    }
    private formaterDateMax(date) {
        const dateButoir = new Date(date);
        console.log(`Nous sommes le : ${dateButoir}`);
        dateButoir.setFullYear(dateButoir.getFullYear() - 18);
        console.log(`L'année de naissance maximum est: ${dateButoir.getFullYear()}`);
        let mois = '' + (dateButoir.getMonth() + 1),
            jour = '' + dateButoir.getDate(),
            annee = dateButoir.getFullYear();
        if (mois.length < 2) mois = '0' + mois;
        if (jour.length < 2) jour = '0' + jour;
        console.log(`La date complète maximum est: ${[annee, mois, jour].join('-')}`);
        return new Date([annee, mois, jour].join('-'));
    }
    private validerSection(strEtape, intSection)
    {
        if(this.arrEtapes[strEtape].indexOf(false) == -1)
        {
            this.barreEtape.activerBoutonSuivant(intSection);
        }
        else
        {
            this.barreEtape.desactiverBoutonSuivant(intSection)
        }

    }


}