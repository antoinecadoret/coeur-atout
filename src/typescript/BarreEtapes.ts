/**
 * Structure HTML du Formulaire Multi-Étapes
 *
 * form.formMultiStep
 *      div ou nav (si navigable) > ol.stepsBar > li.stepsBar__title#step1title
 *      // une section pour chaque étape de formulaire
 *      section[aria-labelledby ='step1title']
 */

export class BarreEtapes {

    private formulaire:HTMLFormElement = document.querySelector('.formMultiSteps');
    private stepsBar:HTMLElement;

    // Liste des titres (et numéros) d'étapes dans la Barre de progression d'Étapes
    private arrStepsBarTitle: Array<HTMLElement>;
    
    // Liste des conteneurs d'étapes du formulaire <section>
    private arrSteps: Array<HTMLElement> = Array.apply(null, document.querySelectorAll('.formMultiSteps__step'));
    
    private etapeEnCours:number = 0;  // indice 0 = Étape 1
    private arrEtapeFini:any = new Array(false,false,false);

    private boutonSubmit:HTMLButtonElement;
    private boutonSuivant:HTMLButtonElement;
    // private boutonPrecedent:HTMLButtonElement;

    private etapePrecedente:number = 0;
    private sensChangement:number = 0;
    
    
    constructor() {
        // ajouter le HTML de la Barre de progression d'étapes
        this.stepsBar = document.createElement('div');
        this.stepsBar.setAttribute('aria-label', 'Formulaire en plusieurs étapes');
        this.stepsBar.innerHTML = `<ol class="stepsBar">
            <li class="stepsBar__title"  id="step1title"> <span>1.</span> Qui?        </li>
            <li class="stepsBar__title"  id="step2title"> <span>2.</span> Mon profil  </li>
            <li class="stepsBar__title"  id="step3title"> <span>3.</span> Mon compte   </li>
            </ol>`;
        this.formulaire.insertBefore(this.stepsBar, this.arrSteps[0]);
        this.arrStepsBarTitle = Array.apply(null, document.querySelectorAll('.stepsBar__title'));

        // initialiser la progression en étapes appeler une méthode afficherEtapeCourante(noEtape)
        this.afficherEtapeCourante(this.etapeEnCours);
    }

    
    private afficherEtapeCourante(numero: number) {
        // ...
        this.arrSteps.forEach(etape => etape.className = "formMultiSteps__step formMultiSteps__step--is-invisible");
        this.arrSteps[numero].className = "formMultiSteps__step formMultiSteps__step--is-visible";
        this.arrStepsBarTitle[numero].setAttribute('aria-current', 'step');
        this.afficherBoutonSuivant();

       // if(numero>0)
       // {
       //     this.afficherBoutonPrecedent();
       // }
        // ...
    }

    private afficherBoutonSuivant(){
        if(this.boutonSuivant)
        {
            this.boutonSuivant.removeEventListener("click",this.changerEtape.bind(this));
                 this.arrSteps[this.etapePrecedente].removeChild(this.boutonSuivant);
        }
        if(this.etapeEnCours<2)
        {
            this.sensChangement=1;
            this.boutonSuivant=document.querySelector('.suivant .formMultiSteps__btn[type=button]');
            this.desactiverBoutonSuivant(this.etapeEnCours);

            this.arrSteps[this.etapeEnCours].appendChild(this.boutonSuivant);
            this.boutonSuivant.addEventListener("click", this.changerEtape.bind(this));
        }
        else
        {
            console.log("dernier etape");
            this.boutonSubmit = document.querySelector('.formMultiSteps__btn[type=submit]');
            console.log(this.boutonSubmit);
            this.desactiverBoutonSuivant(this.etapeEnCours);
            this.arrSteps[this.etapeEnCours].appendChild(this.boutonSubmit);
            this.boutonSubmit.addEventListener("click", this.changerEtape.bind(this));
        }


    }


    private changerEtape(evenement){
        console.log("allo");
        console.log(this.sensChangement);
        this.arrStepsBarTitle[this.etapeEnCours].removeAttribute('aria-current');
        this.arrStepsBarTitle[this.etapeEnCours].className="stepsBar__title is-completed";
        this.etapePrecedente =this.etapeEnCours;
        this.etapeEnCours= this.etapeEnCours + this.sensChangement;
        console.log(this.etapeEnCours);
        this.afficherEtapeCourante(this.etapeEnCours);
    }

    
    public activerBoutonSuivant(numeroEtapeFinie:number) {
        console.log(this.etapeEnCours);
            if(this.etapeEnCours<2)
            {
                this.boutonSuivant.removeAttribute('disabled');

                this.arrEtapeFini[numeroEtapeFinie]=true;
            }
            else
            {
                this.boutonSubmit.removeAttribute('disabled');

                this.arrEtapeFini[this.etapeEnCours]=true;
            }
    }
    public desactiverBoutonSuivant(numEtape){
        console.log("dedans");
        if(numEtape<2)
        {
            this.boutonSuivant.setAttribute('disabled', 'true');
        }
        else
        {
            this.boutonSubmit.setAttribute('disabled', 'true');
        }
        // bouton.setAttribute('disabled', 'true');
    }

}